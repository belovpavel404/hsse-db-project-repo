update clothes
set price = 9990
where item_id = 0;
update clothes
set price = 19990
where item_id = 4;
update clothes
set price = 6490
where item_id = 18;
update clothes
set size = 'XL'
where item_id = 18;

delete
from entered
where item_id = 11
  and entrance_date = '2024-01-11 18:20:00';
delete
from sold
where item_id = 9
  and sale_date = '2024-01-10 19:10:00';
