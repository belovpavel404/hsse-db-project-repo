-- Количество позиций одежды с определённым размером
select size, count(*) as amount
from clothes
group by size
order by amount desc;

-- Сколько штук одежды поступило с определённым размером
select size, sum(Entered.Amount) as total
from clothes
join Entered on clothes.Item_ID = Entered.Item_ID
group by size
order by total desc;

-- Сколько штук одежды в наличии с определённым размером
with total_entered as (
select Item_ID, sum(Entered.Amount) as total
from entered
group by Item_ID
),

total_sold as (
select Item_ID, sum(Sold.Amount) as total
from sold
group by Item_ID
)

select size, sum(total_entered.total) - sum(coalesce(total_sold.total, 0)) as in_stock
from clothes
join total_entered on clothes.Item_ID = total_entered.Item_ID
left join total_sold on clothes.Item_ID = total_sold.Item_ID
group by size
order by in_stock desc;

-- Ранжирование позиций одежды по популярности внутри групп по типу
with total_sold as (
select Item_ID, sum(Sold.Amount) as total
from sold
group by Item_ID
)

select type, brand, name, size, dense_rank() over (
    partition by type
    order by coalesce(total_sold.total, 0) desc
    ) as popularity
from clothes
left join total_sold on Clothes.Item_ID = total_sold.Item_ID
order by Type, popularity;

-- Вывод всех позиций с типом Верхняя одежда
select Brand, name, size
from clothes
where Type = 'Верхняя одежда';

-- Вывод всех позиций, которые поступили в магазин 8 и 9 января 2024 года
select type, brand, name, size
from clothes
join Entered on clothes.Item_ID = Entered.Item_ID
where Entrance_Date between '2024-01-08 00:00:00' and '2024-01-09 23:59:59';

-- Какие позиции одежды брали люди 10 января
select type, brand, name, size
from clothes
join sold on clothes.Item_ID = sold.Item_ID
where DATE(sold.Sale_date) = '2024-01-10';

-- Вывод покупателей, которые купили хотя бы 2 вещи
select First_name, Last_name
from customers
where Customer_ID in (
    select Customer_ID
    from Sold
    group by Customer_ID
    having  sum(Amount) >= 2
);

-- Вывод всех распроданных позиций
with total_entered as (
select Item_ID, sum(Entered.Amount) as total
from entered
group by Item_ID
),

total_sold as (
select Item_ID, sum(Sold.Amount) as total
from sold
group by Item_ID
)

select Type, Brand, Name, Size
from clothes
join total_entered on clothes.Item_ID = total_entered.Item_ID
left join total_sold on clothes.Item_ID = total_sold.Item_ID
where total_entered.total - coalesce(total_sold.total, 0) = 0;

-- Выручка с позиций, которые были проданы
select Sold.Item_ID, Clothes.Price * Sold.Amount as profit
from sold
join clothes on sold.Item_ID = clothes.Item_ID
order by profit desc;

-- Общая выручка
select sum(Clothes.Price * Sold.Amount) as total_profit
from sold
join clothes on sold.Item_ID = clothes.Item_ID;

-- Максимальное время между продажами
with gaps as (select sale_date - lag(Sale_date) over () as time
from sold )

select max(time)
from gaps;

-- Максимальное время между покупками у каждого покупателя, либо null
with gaps_of_customers as (select Customer_ID, Sale_date - lag(Sale_date) over (partition by Customer_ID) as time_beetwen_buys
from sold)

select Customer_ID, max(time_beetwen_buys) as max_gap
from gaps_of_customers
group by Customer_ID;

-- Ранжирование по цене внутри групп по типу
select type, Name, Brand, size, dense_rank() over ( partition by Type
    order by Price desc) as cost_rate
from clothes
order by Type, cost_rate;

-- Рейтинг покупателей по сумме трат
with customer_spend as (
    select Customer_ID, sum(amount*Price) as spending
    from Sold
    join clothes on Sold.Item_ID = clothes.Item_ID
    group by Customer_ID
)

select Customer_ID, dense_rank() over (order by spending desc)
from customer_spend;