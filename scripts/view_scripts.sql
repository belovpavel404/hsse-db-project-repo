-- Вью с общим количеством продаж по позициям
create view total_sold as
select Item_ID, sum(Sold.Amount) as total
from sold
group by Item_ID;

-- Вью с общим количеством поступлений по позициям
create view total_entered as
select Item_ID, sum(Entered.Amount) as total
from entered
group by Item_ID;

-- Вью со скрытыми данными пользователя
create view secured_customers as
select Customer_ID, First_name, concat(substr(Email, 1, 1), repeat('*',strpos(Email, '@') - 1), substr(Email, strpos(Email, '@'), length(Email)))
from customers;

-- количество штук по каждой позиции в наличии
create view in_stock as
select Clothes.Item_ID, total_entered.total - coalesce(total_sold.total, 0) as in_stock
from clothes
join total_entered on clothes.Item_ID = total_entered.Item_ID
left join total_sold on clothes.Item_ID = total_sold.Item_ID
order by Item_ID;

-- Выручка по позициям
create view profit_per_item as
select Sold.Item_ID, sum(Clothes.Price * Sold.Amount) as profit
from sold
join clothes on sold.Item_ID = clothes.Item_ID
group by Sold.Item_ID
order by Item_ID desc;