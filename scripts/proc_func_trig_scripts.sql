create or replace procedure add_purchase(item_id integer, amount integer, customer_id integer, sale_date timestamp)
language plpgsql
as
$$
begin
    insert into sold values (item_id, sale_date, amount, customer_id);

    commit;
end;
$$;

create or replace procedure handle_customer(email text, first_name text, last_name text)
language plpgsql
as
$$
declare max_id integer := (select max(customer_id) over () as max from customers);
begin
    if (email not in (select customers.email from customers)) then
        insert into customers values (email, first_name, last_name, max_id + 1);
    end if;
    commit;
end;
$$;

create or replace procedure add_entering(item_id integer, entrance_date timestamp, amount integer)
language plpgsql
as
$$
begin
    insert into entered values (item_id, entrance_date, amount);
    commit;
end;
$$;

create or replace procedure handle_clothes(type_ text, brand_ text, name_ text, size_ text, price_ integer)
language plpgsql
as
$$
declare declare max_id integer := (select max(item_id) over () as max from clothes);
begin
    if (not exists(select 1 from clothes where
    Clothes.Type = type_ and
    Clothes.Brand = brand_ and
    Clothes.Name = name_ and
    Clothes.Size = size_ and
    Clothes.Price = price_)) then
        insert into clothes values (type_, brand_, name_, size_, price_, max_id + 1);
    end if;
end;
$$;