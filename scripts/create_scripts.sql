create table Customers
(
    Email       text not null,
    First_name  text not null,
    Last_name   text,
    Customer_ID integer not null unique,
    constraint pk_customers_customer_id primary key (Customer_ID)
);

create table Clothes
(
    Type    text check ( Type in ('Верхняя одежда', 'Свитеры и свитшоты', 'Джинсы, брюки и шорты', 'Футболки, поло') ),
    Brand   text not null,
    Name    text,
    Size    text check ( Size in ('XS', 'S', 'M', 'L', 'XL', 'XXL') or Size like('W__, L__')),
    Price   integer check ( Price >= 0 ),
    Item_ID integer not null unique,
    constraint pk_clothes_item_id primary key (Item_ID)
);

create table Entered
(
    Item_ID       integer not null,
    Entrance_Date timestamp not null,
    Amount        integer check ( Amount >= 1 ),
    constraint fk_entered_item_id foreign key (Item_ID) references Clothes (Item_ID)
);

alter table Entered
    ADD PRIMARY KEY (Item_ID, Entrance_Date);

create table Sold
(
    Item_ID     integer not null,
    Sale_date   timestamp not null,
    Amount      integer check ( Amount >= 1 ),
    Customer_ID integer not null,
    constraint fk_sold_customer_id foreign key (Customer_ID) references Customers (Customer_ID),
    constraint fk_sold_item_id foreign key (Item_ID) references Clothes (Item_ID)
);

alter table Sold
    ADD PRIMARY KEY (Item_ID, Sale_date);